# [Security by Default: Intro](https://wldhx.gitlab.io/security-by-default-intro)

An opening talk for *Security by Default* series. Introduces the infosec problem, mostly consists of cases review.

## Building

To build a local version of this deck, use

```sh
git clone https://gitlab.com/wldhx/security-by-default-intro.git
cd security-by-default-intro
yarn install
yarn run start # to start a local dev server
yarn run build # to build a static version
```
