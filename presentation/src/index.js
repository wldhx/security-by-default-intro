import React from 'react';

import {
  Appear, BlockQuote, Cite, Deck,
  Heading, Link, ListItem, List, Quote, Slide,
  Spectacle, Text,
} from 'spectacle';

import createTheme from 'spectacle/lib/themes/default';

require('normalize.css');
require('spectacle/lib/themes/default/index.css');

const theme = createTheme({
  primary: 'rgb(60, 60, 60)',
  secondary: 'hotpink',
  tertiary: 'white',
  quartenary: '#8BC34A',
});
theme.screen.components.list.listStylePosition = 'outside';
theme.screen.components.list.marginRight = '40px';
theme.screen.components.codePane.pre.margin = '20px auto';
theme.screen.components.codePane.pre.fontSize = '1.1rem';
theme.screen.components.heading.h3.color = '#8BC34A'; // FIXME: quartenary

export default () =>
  <Spectacle theme={theme}>
    <Deck transition={['zoom', 'slide']} transitionDuration={500} progress="bar">
      <Slide>
        <Heading size={1} fit>Security</Heading>
        <Heading size={2} fit>by default</Heading>
        <Heading size={3}>Intro, cases</Heading>
      </Slide>
      <Slide>
        <Heading size={1} fit>It would seem we have a problem...</Heading>
        <List>
          <ListItem>Number of interconnected general-purpose computers rising => complexity++</ListItem>
          <ListItem>A *lot* of very real incidents: think Stuxnet, Toyota, Mobile Fortress...</ListItem>
          <ListItem>It's not going to get better</ListItem>
        </List>
      </Slide>
      <Slide>
        <Heading size={1}>Epic Failures</Heading>
        <Heading size={2}>aka Security Nightmares</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Stuxnet</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Toyota</Heading>
        <BlockQuote>
          <Quote>Stack overflow, MISRA-C violation, CPU doesn't incorporate memory protection. "House of cards" security.</Quote>
          <Link href="http://www.edn.com/design/automotive/4423428/Toyota-s-killer-firmware--Bad-design-and-its-consequences"><Cite>Michael Barr, primary expert witness</Cite></Link>
        </BlockQuote>
      </Slide>
      <Slide bgColor="secondary">
        <Link href="https://citizenlab.org/2016/08/million-dollar-dissident-iphone-zero-day-nso-group-uae/"><Heading size={1}>
          NSO Group
        </Heading></Link>
        <BlockQuote>
          <Quote>a government-exclusive “lawful intercept” spyware product ... would have remotely jailbroken Mansoor's stock iPhone</Quote>
        </BlockQuote>
      </Slide>
      <Slide bgColor="secondary">
        <Link href="https://www.theregister.co.uk/2017/02/28/cloudpets_database_leak/"><Heading size={1}>
          Cloudpets
        </Heading></Link>
        <BlockQuote>
          <Quote>unauthenticated MongoDB instance ... ignore alerts ... database held for ransom</Quote>
        </BlockQuote>
      </Slide>
      <Slide bgColor="secondary">
        <Link href="https://motherboard.vice.com/en_us/article/camera-dildo-svakom-siime-eye-hacked-livestream"><Heading size={1}>
          Svakom / Vibe
        </Heading></Link>
        <BlockQuote>
          <Quote>first dildo hack that could potentially expose live footage of someone's most intimate parts</Quote>
        </BlockQuote>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Therac-25</Heading>
      </Slide>
      <Slide>
        <Heading size={1}>Why does this happen?</Heading>
      </Slide>
      <Slide>
        <List>
          <ListItem>Centralization (S3, Cloudbleed, censorship)</ListItem>
          <ListItem>Irresponsible engineering: abstractions over (bad) abstractions, over-backwards compat (C++, OpenSSL, TLS, X11), lack of it (Linux ABI)</ListItem>
          <ListItem>(Cloud control (Garadget, Powerwall, Gapps, Baseband, ME))</ListItem>
        </List>
      </Slide>
      <Slide>
        <List>
          <ListItem>Unsafe tools: PLs (* mainstream), APIs (OpenSSL)</ListItem>
          <ListItem>Lack of testing / review: "seems to work"</ListItem>
        </List>
      </Slide>
      <Slide>
        <List>
          <ListItem>Business cares about features, not good code (GN7)</ListItem>
          <ListItem>Market incentives: it's easier to release a new product every %d than to engineer one that would last (* smartphone)</ListItem>
        </List>
      </Slide>
      <Slide>
        <Heading size={1}>What do we do?</Heading>
      </Slide>
      <Slide>
        <Heading size={1}>Boils down to:</Heading>
        <List>
          <ListItem>Raise awareness, in both devs and managers</ListItem>
          <ListItem>Safer tools / foundations</ListItem>
          <ListItem>(Also, some propose regulation)</ListItem>
        </List>
        <Appear><Heading size={3}>More in next decks!</Heading></Appear>
      </Slide>
      <Slide>
        <Heading fit size={1}>Questions?</Heading>
        <Heading fit size={3} textColor="quartenary">Shall anyone need me,</Heading>
        <Link href="mailto:wldhx+sbd1@wldhx.me"><Heading fit size={2} textColor="secondary">wldhx+sbd1@wldhx.me</Heading></Link>
        <Heading fit size={6} textColor="secondary">FB52 7CDA C117 6535 A4CF 9B4C 0E8C B6EC 231E DDFE</Heading>
        <Text textColor="tertiary">(Also, look up Shodan and @internetofshit)</Text>
      </Slide>
    </Deck>
  </Spectacle>;
